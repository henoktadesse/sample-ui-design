import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCircleAvatar extends StatelessWidget {

  final int animationDuration;
  final double radius;
  final String imagePath;
  final String letterInitial;


  const CustomCircleAvatar({
    Key key,
    this.animationDuration,
    this.radius,
    this.imagePath,
    this.letterInitial
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new AnimatedContainer(
      duration: new Duration(
        milliseconds: animationDuration,
      ),
      constraints: new BoxConstraints(
        minHeight: radius,
        maxHeight: radius,
        minWidth: radius,
        maxWidth: radius,
      ),
      child: new ClipOval(
          child: CircularProfileAvatar(
            imagePath, //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
            radius: radius, // sets radius, default 50.0
            backgroundColor: Colors.transparent, // sets background color, default Colors.white
            borderWidth: 0.1,  // sets border, default 0.0
            initialsText: Text(
              letterInitial!=null?letterInitial:'',
              style: TextStyle(fontSize: 20, color: Colors.white54, fontWeight: FontWeight.bold),
            ),  // sets initials text, set your own style, default Text('')
            borderColor: Colors.grey[850], // sets border color, default Colors.white
            elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
            foregroundColor: Colors.grey[850], //sets foreground colour, it works if showInitialTextAbovePicture = true , default Colors.transparent
            cacheImage: true, // allow widget to cache image against provided url
            showInitialTextAbovePicture: false, // setting it true will show initials text above profile picture, default false
          )
      ),
    );
  }

}
