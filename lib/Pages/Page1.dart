import 'package:custom_app_bar/Widgets/CustomCircleAvatar.dart';
import 'package:flutter/material.dart';

class Page1 extends StatefulWidget {
  const Page1({Key key}) : super(key: key);

  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  String selectedText = '';
  String bottomSelectedText = '';
  int pageIndex = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [

            IndexedStack(
              index: pageIndex,
              children: [

                getGoalsPage(),//index 0
                getProfilePage(),//index 1
                getSummaryPage()//index 2

              ],
            ),

            Spacer(),
            Stack(
              children: [

                Padding(
                  padding: const EdgeInsets.only(top : 60.0),
                  child: Material(
                    borderRadius: BorderRadius.circular(20),
                    elevation: 12,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal : 48.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          getBottomButtonWithIcon('Goals', Icons.account_balance_sharp,0),
                          // getBottomButtonWithIcon('Sad', Icons.account_balance_sharp),
                          getBottomButtonWithIcon('Summary', Icons.account_balance_sharp,2),
                        ],
                      ),
                    ),
                  ),
                ),




                Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: (){
                      setState(() {
                        this.bottomSelectedText = 'Sad';
                        this.pageIndex = 1;
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(Icons.person_pin_circle_rounded, color: ('Sad' == bottomSelectedText)?Colors.blue: Colors.black, size: 80,),
                          ),
                          Text('Sad', style: TextStyle(color: ('Sad' == bottomSelectedText)?Colors.blue : Colors.black,))
                        ],
                      ),
                    ),
                  ),
                )


              ],
            )




          ],
        ),
      ),
    );
  }

  Widget getButtonWithIcon(String text, IconData iconData){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        setState(() {
          this.selectedText = text;
        });
      },
      child: Material(
        borderRadius: BorderRadius.circular(12),
        elevation: 12,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(iconData, color: (text == selectedText)?Colors.blue: Colors.black,),
              ),
              Text(text, style: TextStyle(color: (text == selectedText)?Colors.blue : Colors.black,))
            ],
          ),
        ),
      ),
    );
  }




  Widget getBottomButtonWithIcon(String text, IconData iconData, int index){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        setState(() {
          this.bottomSelectedText = text;
          this.pageIndex = index;
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(iconData, color: (text == bottomSelectedText)?Colors.blue: Colors.black,),
            ),
            Text(text, style: TextStyle(color: (text == bottomSelectedText)?Colors.blue : Colors.black,))
          ],
        ),
      ),
    );
  }







  Widget getGoalsPage(){
    return Center(child: Text('Goals Page'));
  }

  Widget getSummaryPage(){
    return Center(child: Text('Summary Page'));
  }


  Widget getProfilePage(){
    return Column(
      children: [

        Padding(
          padding: const EdgeInsets.symmetric(horizontal : 18.0, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(icon: Icon(Icons.person), onPressed: (){

              }),

              Text('eRehab', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)
              ,
              IconButton(icon: Icon(Icons.chat), onPressed: (){

              }),


            ],
          ),
        ),

        CustomCircleAvatar(
          animationDuration: 100,
          imagePath: 'https://via.placeholder.com/300',
          radius: 100,
          letterInitial: 'e',
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Welcome Josh'),
        ),


        Padding(
          padding: const EdgeInsets.symmetric(vertical : 48.0),
          child: Material(
            borderRadius: BorderRadius.circular(12),
            elevation: 12,
            child: FractionallySizedBox(
              widthFactor: 0.7,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical : 18.0),
                child: Column(
                  children: [

                    Text('Well Done', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),

                    Icon(
                        Icons.flag
                    ),

                    Text('Last week\'s \ngoals achieved', textAlign: TextAlign.center,),

                    Padding(
                      padding: const EdgeInsets.only( top : 18.0),
                      child: Text('94%', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
                    )

                  ],
                ),
              ),
            ),
          ),
        ),



        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('How are you feeling today ?', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19, color: Colors.black.withOpacity(0.6)),),
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            getButtonWithIcon('Happy', Icons.ac_unit),
            getButtonWithIcon('Sad', Icons.ac_unit),
            getButtonWithIcon('Motivated', Icons.ac_unit),
            getButtonWithIcon('Not Motivated', Icons.ac_unit),
          ],
        ),



      ],
    );
  }

}
